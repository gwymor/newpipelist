# newpipelist

A utility to convert playlists in a [NewPipe](https://newpipe.net/) database export to M3U files.

- Install: `pip install newpipelist`
- [Source Code (Git)](https://codeberg.org/gwymor/newpipelist.git): `git clone https://codeberg.org/gwymor/newpipelist.git`
- Requirements: sqlite3

Send patches to gwymor AT tilde DOT club.

## Usage

Export your NewPipe data and use this program with the `newpipe.db` file.

``` sh
# List playlists
newpipelist --list newpipe.db

# Export a playlist by its ID
newpipelist --playlist 123 newpipe.db >my-playlist.m3u

# Export all playlists to a directory
newpipelist --directory playlists newpipe.db
```

## License

MIT
